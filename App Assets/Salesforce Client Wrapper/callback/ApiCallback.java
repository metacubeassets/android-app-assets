package com.metacube.twg.app.api.callback;

import com.metacube.twg.app.api.ApiError;

public interface ApiCallback {
    void onSuccess(Object response);
    void onFailure(ApiError t);
}
