package com.metacube.twg.app.api;

import android.app.Activity;

import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.metacube.twg.BuildConfig;
import com.metacube.twg.app.api.callback.ApiCallback;
import com.metacube.twg.app.api.convertor.LocalDateTypeConverter;
import com.salesforce.androidsdk.app.SalesforceSDKManager;
import com.salesforce.androidsdk.rest.ClientManager;
import com.salesforce.androidsdk.rest.RestClient;
import com.salesforce.androidsdk.rest.RestRequest;
import com.salesforce.androidsdk.rest.RestResponse;

import org.joda.time.LocalDate;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.UnknownHostException;

import okhttp3.logging.HttpLoggingInterceptor;

/**
 * Created by Harshita Ahuja on 11/05/17.
 */

public class MyRestClient {//extends RestClient {
    
    ClientManager clientManager;
    
    public MyRestClient(ClientManager clientManager) {
        this.clientManager = clientManager;
    }
    
    
    public ClientManager getClientManager() {
        return clientManager;
    }

//    public MyRestClient(ClientInfo clientInfo, String authToken, HttpAccess httpAccessor, AuthTokenProvider
//            authTokenProvider) {
//        super(clientInfo, authToken, httpAccessor, authTokenProvider);
//    }
//
//    public MyRestClient(HttpAccess httpAccessor, OAuthRefreshInterceptor httpInterceptor) {
//        super(httpAccessor, httpInterceptor);
//
//    }
    
    
    // ClientManager, AuthProvider
    // new ->
    // Request URL
    // return ParameterTypes = Query/ Body etc
    // return Encoding <- json, object, form
    // return Parameters <- json
    // return ResponseType <- json, object, list of object
    
    private abstract class RestApi {
        String path;
        RestRequest.RestMethod restMethod;
        String urlParameters;
        JSONObject requestBodyAsJson;
    }
    
    
    //Used for authenticated calls after login
    public void asynCall(final Activity activity, final RestRequest restRequest, final ApiCallback apiCallback, final
    Class<?> cls) {
        // if(MyApplication.tinyDB.getBoolean(StringConstants.SHARED_PREFERENCES_IS_LOGGED_IN,false)) {
        clientManager.getRestClient(activity, client -> {
            
            if (BuildConfig.DEBUG) {
                HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
                interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
                client.setOkHttpClient(client.getOkHttpClientBuilder().addNetworkInterceptor(interceptor).build());
                client.setOkHttpClient(client.getOkHttpClientBuilder().addNetworkInterceptor(new StethoInterceptor())
                                               .build());
            }
            
            if (client == null) {
                SalesforceSDKManager.getInstance().logout(activity);
            } else {
                client.sendAsync(restRequest, new RestClient.AsyncRequestCallback() {
                    @Override
                    public void onSuccess(RestRequest request, RestResponse response) {
                        response.consumeQuietly(); // consume before going back to main thread
                        if (response.isSuccess() && response.getStatusCode() == 200) {
                            
                            GsonBuilder gsonBuilder = new GsonBuilder();
                            gsonBuilder.registerTypeAdapter(LocalDate.class, new LocalDateTypeConverter());
                            Gson gson = gsonBuilder.create();
                            try {
                                
                                Object object = gson.fromJson(response.asString(), cls);
                                if (activity != null) {
                                    activity.runOnUiThread(() -> {
                                        apiCallback.onSuccess(object);
                                    });
                                }
                            } catch (IOException | JsonSyntaxException e) {
                                e.printStackTrace();
                                ApiError error = new ApiError("error", e.getMessage());
                                if (activity != null) {
                                    activity.runOnUiThread(() -> {
                                        apiCallback.onFailure(error);
                                    });
                                }
                            }
                            
                        } else {
                            if (activity != null) {
                                activity.runOnUiThread(() -> {
                                    ApiError error = new ApiError("Something went wrong.Please try again later.", "");
                                    try {
                                        
                                        error = new ApiError(response.asJSONArray()
                                                                     .getJSONObject(0).getString("message"), response.asJSONArray()
                                                        
                                                                                                                     .getJSONObject(0).getString("errorCode"));
                                    } catch (JSONException | IOException e) {
                                        e.printStackTrace();
                                    }
                                    
                                    apiCallback.onFailure(error);
                                });
                            }
                        }
                    }
                    
                    @Override
                    public void onError(Exception exception) {
                        if (activity != null) {
                            activity.runOnUiThread(() -> {
                                ApiError error = null;
                                if (exception instanceof UnknownHostException) {
                                    error = new ApiError("No Internet Connection. Please Try Again.", "");
                                } else {
                                    error = new ApiError("Request timed out due to bad network. Please check your " +
                                                                 "internet connection.",
                                                         "");
                                }
                                apiCallback.onFailure(error);
                            });
                        }
                    }
                });
            }
        });
        // }
    }
    
    //Used for authenticated calls after login
    public void asyncCallUnauthenticated(final Activity activity, final RestRequest restRequest, final ApiCallback
                                                                                                         apiCallback,
                                         final
                                         Class<?> cls) {
        
        clientManager
                .peekUnauthenticatedRestClient().sendAsync(restRequest, new RestClient.AsyncRequestCallback() {
            @Override
            public void onSuccess(RestRequest request, RestResponse response) {
                response.consumeQuietly(); // consume before going back to main thread
                if (response.isSuccess() && response.getStatusCode() == 200) {
                    
                    GsonBuilder gsonBuilder = new GsonBuilder();
                    gsonBuilder.registerTypeAdapter(LocalDate.class, new LocalDateTypeConverter());
                    Gson gson = gsonBuilder.create();
                    try {
                        Object object = gson.fromJson(response.asString(), cls);
                        if (activity != null) {
                            activity.runOnUiThread(() -> {
                                apiCallback.onSuccess(object);
                            });
                        }
                    } catch (IOException | JsonSyntaxException e) {
                        e.printStackTrace();
                    }
                    
                } else {
                    if (activity != null) {
                        activity.runOnUiThread(() -> {
                            ApiError error = new ApiError("Something went wrong.Please try again later.", "");
                            try {
                                error = new ApiError(response.asJSONArray()
                                                             .getJSONObject(0).getString("message"), response.asJSONArray()
                                                
                                                                                                             .getJSONObject
                                                                                                                      (0).getString("errorCode"));
                            } catch (JSONException | IOException e) {
                                e.printStackTrace();
                            }
                            
                            apiCallback.onFailure(error);
                        });
                    }
                }
            }
            
            @Override
            public void onError(Exception exception) {
                if (activity != null) {
                    activity.runOnUiThread(() -> {
                        ApiError error = null;
                        if (exception instanceof UnknownHostException) {
                            error = new ApiError("No Internet Connection. Please Try Again.", "");
                        } else {
                            error = new ApiError("Request timed out due to bad network. Please check your " +
                                                         "internet connection.",
                                                 "");
                        }
                        apiCallback.onFailure(error);
                    });
                }
            }
        });
    }
}
