package com.metacube.twg.app.api;

import android.util.Base64;

import com.metacube.twg.app.util.FlavorConstants;
import com.salesforce.androidsdk.rest.RestRequest;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


/**
 * Created by Harshita Ahuja on 13/10/17.
 */

public class Requests {
    
    private static final String BASE_URL_UNAUTHENTICATED = FlavorConstants.BASE_URL_UNAUTHENTICATED;
    private static final String BASE_URL = FlavorConstants.BASE_URL;
    
    
    private static final Map<String, String> additionalHttpHeaders;
    
    static {
        String keyValue = "<value>";
        additionalHttpHeaders = new HashMap<String, String>();
        additionalHttpHeaders.put("AuthCode", Base64.encodeToString(keyValue.getBytes(),
                                                                    Base64.NO_WRAP));
    }
    
    //unauthenticated requests
    public static RestRequest getExampleUnauthenticated(String param) {
        return new RestRequest(RestRequest.RestMethod.GET, BASE_URL_UNAUTHENTICATED
                                                                   + "Example" + param, additionalHttpHeaders);
    }
    
    
    
    //authenticated requests
    
    public static RestRequest getExampleAuthenticated(String param) {
        return new RestRequest(RestRequest.RestMethod.GET,
                               BASE_URL + "Example" + param);
    }
    
   
}