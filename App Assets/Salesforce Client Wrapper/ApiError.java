package com.metacube.twg.app.api;

/**
 * Created by Harshita Ahuja on 11/05/17.
 */

public class ApiError {

    public String mMessage;
    private String mErrorCode;

    public ApiError(String message, String errorCode) {
        this.mMessage = message;
        this.mErrorCode = errorCode;
    }
}
