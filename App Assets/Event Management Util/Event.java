package com.metacube.twg.app.util.eventutil;

/**
 * Created by Harshita Ahuja on 20/11/16.
 */
public class Event {

    public static final int USER_LOGGED_OUT = 91;
    public static final int USER_LOGIN_FAILED = 92;

    public static final int VENDOR_PROFILE_UPDATED = 101;
    public static final int FORCE_UPDATE_VENDOR_PROFILE = 102;

    public static final int PRODUCT_WALLET_UPDATED = 201;
    
    public static final int OPEN_SERVICE_REQUEST_UPDATED = 301;
    public static final int FORCE_UPDATE_OPEN_SERVICE_REQUEST = 302;
    public static final int ACCEPTED_SERVICE_REQUEST_UPDATED = 303;
    public static final int FORCE_UPDATE_ACCEPTED_SERVICE_REQUEST = 304;
    public static final int COMPLETED_SERVICE_REQUEST_UPDATED = 305;
    public static final int FORCE_UPDATE_COMPLETED_SERVICE_REQUEST = 306;
    
    public static final int OPEN_JOBS_LIST_UPDATED = 401;
    public static final int BIDDING_JOBS_LIST_UPDATED = 402;
    public static final int FORCE_UPDATE_OPEN_JOBS = 403;
    public static final int FORCE_UPDATE_BIDDING_JOBS = 404;
    public static final int ACCEPTED_JOBS_LIST_UPDATED = 501;
    public static final int FORCE_UPDATE_ACCEPTED_JOBS = 502;
    public static final int COMPLETED_JOBS_LIST_UPDATED = 601;
    public static final int FORCE_UPDATE_COMPLETED_JOBS = 602;
    
    public static final int NOTIFICATION_LIST_UPDATED = 701;
}
