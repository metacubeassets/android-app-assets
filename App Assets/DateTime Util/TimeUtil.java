package com.metacube.twg.app.util;

import java.util.Date;

/**
 * Created by Harshita Ahuja on 12/6/2017.
 */

public class TimeUtil {

    public final static long ONE_SECOND = 1000;
    public final static long ONE_MINUTE = ONE_SECOND * 60;
    public final static long ONE_HOUR = ONE_MINUTE * 60;
    public final static long ONE_DAY = ONE_HOUR * 24;
    public final static long ONE_WEEK = ONE_DAY * 7;
    public final static long ONE_MONTH = ONE_DAY * 30;
    public final static long ONE_YEAR = ONE_DAY * 365;

    private TimeUtil() {
    }

    /**
     * converts time (in milliseconds) to human-readable format
     *  "<w> days, <x> hours, <y> minutes and (z) seconds"
     */
    public static String millisToLongDHMS(Date date) {
        long duration = new Date().getTime() - date.getTime();
        long temp = 0;

        temp = duration / ONE_YEAR;
        if (temp > 0) {
            return (temp + " Year" + (temp > 1 ? "s" : "") + " ago");
        }

        temp = duration / ONE_MONTH;
        if (temp > 0) {
            return (temp + " Month" + (temp > 1 ? "s" : "") + " ago");
        }

        temp = duration / ONE_WEEK;
        if (temp > 0) {
            return (temp + " Week" + (temp > 1 ? "s" : "") + " ago");
        }

        temp = duration / ONE_DAY;
        if (temp > 0) {
            return (temp + " Day" + (temp > 1 ? "s" : "") + " ago");
        }

        temp = duration / ONE_HOUR;
        if (temp > 0) {
            return (temp + " Hour" + (temp > 1 ? "s" : "") + " ago");
        }

        temp = duration / ONE_MINUTE;
        if (temp > 0) {
            return (temp + " Minute" + (temp > 1 ? "s" : "") + " ago");
        }

        temp = duration / ONE_SECOND;
        if (temp > 0) {
            return (temp + " Second" + (temp > 1 ? "s" : "") + " ago");
        }

        return "Just now";
    }

}