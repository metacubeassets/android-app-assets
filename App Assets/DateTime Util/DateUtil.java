package com.metacube.twg.app.util;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Harshita Ahuja on 9/28/2017.
 */

public class DateUtil {
    
    private static final String[] monthArray = {"Jan", "Feb", "Mar", "Apr", "May", "Jun",
            "Jul", "Aug", "Sept", "Oct", "Nov", "Dec"};
    
    public static String convertIntoAlphaDate(String date) {
        if (date != null) {
            String[] dArray = date.split("-");
            if (dArray.length == 3) {
                return dArray[2] + " " + monthArray[Integer.parseInt(dArray[1]) - 1] + " " + dArray[0];
            }
        }
        return "";
    }
    
    public static String getTime(Date date) {
        if (date != null) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm a");
            return dateFormat.format(date);
        }
        return "";
    }
    
    public static String getDate(Date date) {
        if (date != null) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy");
            return dateFormat.format(date);
        }
        return "";
    }
}
