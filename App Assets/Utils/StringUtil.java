package com.metacube.twg.app.util;

import android.text.Editable;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Harshita Ahuja on 16/11/15.
 */
public class StringUtil {

    public static String toTitleCase(String input) {
        StringBuilder titleCase = new StringBuilder();
        boolean nextTitleCase = true;
        if (input != null && input.length() > 0) {
            for (char c : input.toCharArray()) {
                if (Character.isSpaceChar(c)) {
                    nextTitleCase = true;
                } else if (nextTitleCase) {
                    c = Character.toTitleCase(c);
                    nextTitleCase = false;
                }

                titleCase.append(c);
            }
        }
        return titleCase.toString();
    }

    public static boolean isDigit(String s) {
        String regex = "\\d+";
        return s.matches(regex);
    }

    public static boolean isNotEmpty(String s) {

        return s != null && s.length() > 0;
    }

    public static boolean isAlphanumeric(String s) {
        String n = ".*[0-9].*";
        String a = ".*[A-Za-z].*";
        return s.matches(n) && s.matches(a);
    }

    public static boolean isValidEmail(CharSequence target) {
        return target != null && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public static String twoDecimalPlace(float number){
        return String.format("$%.2f",number);
    }

    public static boolean getSpecialCharacterCount(String s) {
        if (s == null || s.trim().isEmpty()) {
            System.out.println("Incorrect format of string");
            return true;
        }
        Pattern p = Pattern.compile("[^A-Za-z0-9]");
        Matcher m = p.matcher(s);
        // boolean b = m.matches();
        boolean b = m.find();
        return b;
    }

    private static String stripExceptNumbers(String str, boolean includePlus) {
        StringBuilder res = new StringBuilder(str);
        String phoneChars = "0123456789";
        if (includePlus) {
            phoneChars += "+";
        }
        for (int i = res.length() - 1; i >= 0; i--) {
            if (!phoneChars.contains(res.substring(i, i + 1))) {
                res.deleteCharAt(i);
            }
        }
        return res.toString();
    }

    private static String stripExceptNumbers(String str) {
        return stripExceptNumbers(str, false);
    }


    public static Editable formatString(Editable s){
        String st = s.toString();
        s.clear();
        String text = StringUtil.stripExceptNumbers(st);
        if (text.length() == 3) {
            s.insert(0,"(" + text.substring(0,3) + ") ");
        } else if (text.length() == 6) {
            s.insert(0,"(" + text.substring(0,3) + ") " + text.substring(3,6) + "-");
        }
        else if(text.length() == 10){
            s.insert(0,"(" + text.substring(0,3) + ") " + text.substring(3,6) + "-"  + text.substring(6,10) + " ");
        }
        else if(text.length() > 10 && text.length() < 16){
            s.insert(0,"(" + text.substring(0,3) + ") " + text.substring(3,6) + "-"  + text.substring(6,10) + " " + text.substring(10));
        }
        else {
            s.insert(0,st);
        }
        return s;
    }

    public static String getFormattedString(String phone){
        String st ;
        String text = StringUtil.stripExceptNumbers(phone);
        if (text.length() == 3) {
            st = "(" + text.substring(0,3) + ") ";
        } else if (text.length() == 6) {
            st = "(" + text.substring(0,3) + ") " + text.substring(3,6) + "-";
        }
        else if(text.length() == 10){
            st = "(" + text.substring(0,3) + ") " + text.substring(3,6) + "-"  + text.substring(6,10);
        }
        else if(text.length() > 10 && text.length() < 16){
            st = "(" + text.substring(0,3) + ") " + text.substring(3,6) + "-"  + text.substring(6,10) + " " + text.substring(10);
        }
        else {
            st = phone;
        }
        return st;
    }
}
