package com.metacube.twg.app.util;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.metacube.twg.R;

public class PermissionUtil {

    private static final int REQUEST_CALL_PHONE = 1;
    private static final int REQUEST_APP_SETTINGS = 168;
    private static String phoneNumber = "";
    private AppCompatActivity activity;
    private ViewGroup rootLayout;
    private Fragment currentFragment;


    public PermissionUtil(ViewGroup rootLayout, AppCompatActivity currentActivity) {

        this.activity = currentActivity;
        this.rootLayout = rootLayout;
    }

    public PermissionUtil(ViewGroup rootLayout, Fragment currentFragment) {

        this.currentFragment = currentFragment;
        this.rootLayout = rootLayout;
    }

    private static void goToSettings(Activity activity) {
        Intent myAppSettings = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse
                ("package:" + activity.getPackageName()));
        myAppSettings.addCategory(Intent.CATEGORY_DEFAULT);
        myAppSettings.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        activity.startActivityForResult(myAppSettings, REQUEST_APP_SETTINGS);
    }

    private void callHelpNumber(String phone) {
        phoneNumber = phone;
        if(activity!= null) {
            if (Build.VERSION.SDK_INT > 22 &&
                    ActivityCompat.checkSelfPermission(activity, android.Manifest.permission.CALL_PHONE)
                            != PackageManager.PERMISSION_GRANTED) {
                requestCallPermission();
                return;
            }
        }
        else
        {
            if (Build.VERSION.SDK_INT > 22 &&
                    currentFragment.getActivity().checkSelfPermission(android.Manifest.permission.CALL_PHONE)
                            != PackageManager.PERMISSION_GRANTED) {
                requestCallPermission();
                return;
            }
        }

        Intent callIntent = new Intent(Intent.ACTION_CALL);
        String uri = "tel:" + phone;
        callIntent.setData(Uri.parse(uri));
        if(activity != null) {
            activity.startActivity(callIntent);
        }
        else
        {
            currentFragment.getActivity().startActivity(callIntent);
        }
    }

    private void requestCallPermission() {
        if(activity != null) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity,
                    android.Manifest.permission.CALL_PHONE)) {
                // Provide an additional rationale to the user if the permission was not granted
                // and the user would benefit from additional context for the use of the permission.
                // For example if the user has previously denied the permission.

                Snackbar.make(rootLayout, R.string.permission_callphone_rationale,
                        Snackbar.LENGTH_INDEFINITE)
                        .setAction("ok", view -> ActivityCompat.requestPermissions(activity,
                                new String[]{android.Manifest.permission.CALL_PHONE},
                                REQUEST_CALL_PHONE))
                        .show();
            } else {

                // CALL_PHONE permission has not been granted yet. Request it directly.
                ActivityCompat.requestPermissions(activity, new String[]{android.Manifest.permission
                                .CALL_PHONE},
                        REQUEST_CALL_PHONE);
            }
        }
        else
        {

            if (currentFragment.shouldShowRequestPermissionRationale(
                    android.Manifest.permission.CALL_PHONE)) {
                // Provide an additional rationale to the user if the permission was not granted
                // and the user would benefit from additional context for the use of the permission.
                // For example if the user has previously denied the permission.

                Snackbar.make(rootLayout, R.string.permission_callphone_rationale,
                        Snackbar.LENGTH_INDEFINITE)
                        .setAction("ok", view -> currentFragment.requestPermissions(new String[]{android.Manifest.permission.CALL_PHONE},
                                REQUEST_CALL_PHONE))
                        .show();
            } else {
                currentFragment.requestPermissions(new String[]{android.Manifest.permission.CALL_PHONE},
                        REQUEST_CALL_PHONE);
            }
        }
        // END_INCLUDE(CALL_PHONE_permission_request)
    }

    public void processResult(int requestCode, String[] permissions,
                              int[] grantResults) {
        // BEGIN_INCLUDE(permission_result)
        // Received permission result for Call Phone permission.
        // SmartLog.i(TAG, "Received response for Call Phone permission request.");

        // Check if the only required permission has been granted
        if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            // Call Phone permission has been granted, preview can be displayed
            // SmartLog.i(TAG, "Call Phone permission has now been granted. Showing preview.");
            callHelpNumber(phoneNumber);
        } else {
            // SmartLog.i(TAG, "Call Phone permission was NOT granted.");
            Snackbar snackbar = Snackbar.make(rootLayout, R.string.permission_callphone_not_granted,
                    Snackbar.LENGTH_LONG)
                    .setAction("OPEN SETTINGS", view -> goToSettings());
            // Changing mMessage text color
            //snackbar.setActionTextColor(Color.RED);

            // Changing action button text color
            View sbView = snackbar.getView();
            TextView textView = sbView.findViewById(android.support.design.R.id
                    .snackbar_text);
            textView.setTextColor(Color.YELLOW);
            snackbar.show();

        }
        // END_INCLUDE(permission_result)
    }


    // Static ListUtil Methods

    private void goToSettings() {
        Intent myAppSettings = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse
                ("package:" + activity.getPackageName()));
        myAppSettings.addCategory(Intent.CATEGORY_DEFAULT);
        myAppSettings.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        activity.startActivityForResult(myAppSettings, REQUEST_APP_SETTINGS);
    }

    public static void showSnackbarForSettings(Activity activity, View rootLayout, int
    message) {
        Snackbar snackbar = Snackbar.make(rootLayout, message,
                Snackbar.LENGTH_LONG)
                .setAction("OPEN SETTINGS", (View view) -> {
                    goToSettings(activity);
                });
        // Changing mMessage text color
        //snackbar.setActionTextColor(Color.RED);

        // Changing action button text color
        View sbView = snackbar.getView();
        TextView textView = sbView.findViewById(android.support.design.R.id
        .snackbar_text);
        textView.setTextColor(Color.YELLOW);
        snackbar.show();
    }

}

