package com.metacube.twg.app.util;

import com.metacube.twg.app.MyApplication;

/**
 * Created by Harshita Ahuja on 12/15/2017.
 */

public class CommonUtil {

    public static void runOnUIThread(Runnable runnable) {
        MyApplication.applicationHandler.post(runnable);
    }
}
