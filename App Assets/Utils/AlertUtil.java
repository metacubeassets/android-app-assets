package com.metacube.twg.app.util;

import android.app.Activity;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.view.View;

import com.metacube.twg.R;
import com.metacube.twg.app.MyApplication;

/**
 * Created by Harshita Ahuja on 22/09/17.
 */

public class AlertUtil {
    
    public static void showSnackbarWithOkButton(String message, View view) {
        final Snackbar snackbar = Snackbar.make(view, message,
                Snackbar.LENGTH_LONG).setAction(MyApplication.getApp().getString(R.string.ok), view1 -> {
        });
        snackbar.show();
    }

    public static void showSnackbar(String message, View view) {
        Snackbar.make(view, message, Snackbar.LENGTH_LONG).show();
    }

    public static void callAlert(Activity activity, String message) {
        new AlertDialog.Builder(activity)
                .setCancelable(false)
                .setMessage(message)
                .setPositiveButton(MyApplication.getApp().getString(R.string.ok),
                        (dialog, whichButton) -> dialog.dismiss()).create().show();
    }
}
