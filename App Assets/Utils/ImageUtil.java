package com.metacube.twg.app.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;


/**
 * Created by Harshita Ahuja on 7/25/2017.
 */

public class ImageUtil {
    
    private static String root = Environment.getExternalStorageDirectory().toString() + "/TheHelmImages";
    private static File mydir = null;
    private static String product = "product_";
    private static String image= "image";
    private static String profile = "profile";

    public static File getRootDirectory() {
        if (mydir == null) {
            mydir = new File(root);
            mydir.mkdirs();
        }
        return mydir;
    }
    
    private static File getProductDirectory(String productName) {
        File innerDir = new File(root + "/" + product + "_" + productName);
        if (!innerDir.exists())
            innerDir.mkdirs();
        return innerDir;
    }
    
    public static int getProductDirectoryCount(String productName) {
        File innerDir = new File(root + "/" + product + "_" + productName);
        if (!innerDir.exists())
            return 0;
        return innerDir.listFiles().length;
    }
    
    public static File getImage(String productName, int imageNumber) {
        File file = new File(root + "/" + product + "_" + productName + "/" + image + imageNumber);
        if (!file.exists())
            return null;
        return file;
    }
    
    private static File getProfileDirectory() {
        File innerDir = new File(root + "/" + profile);
        if (!innerDir.exists())
            innerDir.mkdirs();
        return innerDir;
    }
    
    public static Uri resizeImage(Uri sourceImage, String productName, long imageNumber) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        Bitmap bitmap = BitmapFactory.decodeFile(sourceImage.getPath(), options);
        
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        long lengthbmp = 0;
        int quality = 100;
        try {
            do {
                bitmap.compress(Bitmap.CompressFormat.JPEG, quality, stream);
                lengthbmp = stream.toByteArray().length / 1024;
                quality -= 10;
            } while (lengthbmp > 1024 && quality > 40);
        } catch (OutOfMemoryError e) {
            return null;
        }
        
        File folder, file;
        if (imageNumber != 0) {
            folder = getProductDirectory(productName);
            file = new File(folder, image + imageNumber + ".jpg");
        } else {
            folder = getProfileDirectory();
            file = new File(folder, profile);
            
            if (file.exists())
                file.delete();
        }
        
        try {
            FileOutputStream out = new FileOutputStream(file, false);
            if (quality == 40) quality += 10;
            bitmap.compress(Bitmap.CompressFormat.JPEG, quality, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("1234", "crash");
            return null;
        }
        return Uri.fromFile(file);
    }
    
    public static void deleteFile(Uri uri) {
        File file = new File(uri.getPath());
        if (file.exists())
            file.delete();
    }
    
}
