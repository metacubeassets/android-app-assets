package com.metacube.twg.app.util;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AlertDialog;

import com.metacube.twg.R;
import com.metacube.twg.app.MyApplication;


public class NetworkUtil {
    
    private static int TYPE_WIFI = 1;
    private static int TYPE_MOBILE = 2;
    private static int TYPE_NOT_CONNECTED = 0;
    
    public static boolean isNetworkAvailable() {
        ConnectivityManager connectivity = (ConnectivityManager) MyApplication.appContext
                                                                         .getSystemService(Context.CONNECTIVITY_SERVICE);
        
        if (connectivity != null) {
            NetworkInfo activeNetwork = connectivity.getActiveNetworkInfo();
            if (null != activeNetwork && activeNetwork.isConnected()) {
                return true;
            }
        }
        return false;
    }
    
    public static int getConnectivityStatus(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context
                                                               .getSystemService(Context.CONNECTIVITY_SERVICE);
        
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (null != activeNetwork) {
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI)
                return TYPE_WIFI;
            
            if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE)
                return TYPE_MOBILE;
        }
        return TYPE_NOT_CONNECTED;
    }
    
    public static void callNoNetworkAlert(Activity activity) {
        if (activity != null) {
            new AlertDialog.Builder(activity)
                    .setCancelable(false)
                    .setMessage(MyApplication.getApp().getString(R.string.noInternetConnection))
                    .setPositiveButton(MyApplication.getApp().getString(R.string.ok),
                            (dialog, whichButton) -> dialog.dismiss()).create().show();
        }
    }
    
}
